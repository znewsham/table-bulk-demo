import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
const MyData = new Meteor.Collection("MyData");

import './main.html';
Template.tableCheckbox.helpers({
  bulkActionCheckboxOptions(){
    return _.extend({
      style: {
        dropdown: "width: 250px;"
      },
      options: _.compact([
        this.selectAll ? {
          label: "Export",
          callback(docIds){
            $(this).closest("table").data("do-export")({selectedIds: docIds});
          }
        } : null,
        {
          label: "Make Blue",
          callback(target, value){
            (_.isArray(target) ? target : [target]).forEach(targetId => MyData.update({_id: targetId}, {$set: {color: "blue"}}));
          }
        },
        {
          label: "Make Green",
          callback(target, value){
            (_.isArray(target) ? target : [target]).forEach(targetId => MyData.update({_id: targetId}, {$set: {color: "green"}}));
          }
        },
        {
          label: "Assign to 'Test User'",
          callback(target, value){
            (_.isArray(target) ? target : [target]).forEach(targetId => MyData.update({_id: targetId}, {$set: {ownerId: Meteor.users.findOne({"profile.firstName":"Test"})._id}}));
          }
        },
        {
          label: "Delete",
          callback(target, value){
            (_.isArray(target) ? target : [target]).forEach(targetId => MyData.remove({_id: targetId}));
          }
        }
      ])
    }, this);
  }
});
Template.main.helpers({
  tableOptions(set){
    return {
      collection: MyData,
      publication: "myData",
      compositePublicationNames: ["myDataOwner"],
      export: {
        
      },
      advancedSearch: {
        beforeRender(){
          this.subscribe("users", {});
        },
        fields: [
          "title",
          "color",
          {
            field: "ownerId",
            label: "Owner",
            options(){
              return Meteor.users.find({}).map(u => ({
                label: `${u.profile.firstName} ${u.profile.lastName}`,
                value: u._id
              }))
            },
            comparators: [
              {label: "Is", operator: ""},
              {label: "Is not", operator: "$ne"}
            ]
          },
        ]
      },
      columns: [
        {
          sortable: false,
          titleTmpl: Template.tableCheckbox,
          titleTmplContext(){
            return {
              set,
              title: "Bulk",
              selectAll: true
            }
          },
          tmpl: Template.tableCheckbox,
          tmplContext(rowData){
            return {
              target: rowData._id,
              set,
              selectAll: false
            }
          }
        },
        {
          data: "title",
          title: "Name"
        },
        {
          data: "color",
          title: "Color"
        },
        {
          data: "ownerId",
          title: "Owner",
          search(query){
            return (_.isArray(query) ? query : [query]).map(q=>({
              ownerId: {$in: Meteor.users.find({$or: [{"profile.firstName": q}, {"profile.lastName": q}]}, {fields: {_id: true}}).map(t=>t._id)}
            }));
          },
          render(val){
            const user = Meteor.users.findOne({_id: val});
            return `${user.profile.firstName} ${user.profile.lastName}`;
          }
        }
      ]
    }
  }
});
