import { Meteor } from 'meteor/meteor';

const MyData = new Meteor.Collection("MyData");
MyData._ensureIndex({ownerId: 1}, {background: true});
MyData._ensureIndex({ownerId: 1, color: 1}, {background: true});
MyData.allow({
  insert(){
    return true;
  },
  update(){
    return true;
  },
  remove(){
    return true;
  }
});
const colors = [
  "red",
  "green",
  "blue"
];
function setupData(){
  Meteor.users.remove({});
  MyData.remove({});
  const user1Id = Meteor.users.insert({
    profile: {
      firstName: "Test",
      lastName: "User"
    }
  });
  const user2Id = Meteor.users.insert({
    profile: {
      firstName: "Another",
      lastName: "Account"
    }
  });

  for(var i = 0; i < 1000; i++){
    MyData.insert({
      title: `Document ${i}`,
      ownerId: Math.random() < 0.5 ? user1Id : user2Id,
      color: colors[Math.floor(Math.random() * colors.length)]
    });
  }
}


Meteor.startup(() => {
  if(MyData.find().count() === 0){
    setupData();
  }
});

Meteor.publish("myData", function(selector, options){
  return MyData.find(selector, options);
});

Meteor.publish("myDataOwner", function(data){
  return Meteor.users.find({_id: data.ownerId});
});
